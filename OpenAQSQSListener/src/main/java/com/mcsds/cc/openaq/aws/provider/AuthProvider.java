package com.mcsds.cc.openaq.aws.provider;

import com.amazonaws.auth.AWSCredentials;

public interface AuthProvider {
	
	static String DEFAULT_PROVIDER = "default";
	AWSCredentials getAWSCredentials(String provider);
	
}
