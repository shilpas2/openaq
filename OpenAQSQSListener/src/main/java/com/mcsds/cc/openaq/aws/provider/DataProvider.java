package com.mcsds.cc.openaq.aws.provider;

import org.springframework.core.io.Resource;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.sqs.AmazonSQS;

public interface DataProvider {
	
	AmazonS3 s3Initializer ();
	AmazonSQS sqsInitializer ();
	Resource s3ResourceLoader ();
	
}
