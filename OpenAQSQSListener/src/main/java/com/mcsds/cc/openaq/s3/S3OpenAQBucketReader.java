package com.mcsds.cc.openaq.s3;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import com.mcsds.cc.openaq.constants.DataConstants;

@RestController
public class S3OpenAQBucketReader {

	private Logger logger = Logger.getLogger(this.getClass().getName());

	@Value("us-east-1")
	private String region;

	@Value("openaq-fetches")
	private String bucket;

	@Value("realtime")
	private String filter;

	@Value("true")
	private String isKafkaEnabled;

	@Autowired(required = true)
	private AmazonS3 s3Initializer;

	@Autowired(required = true)
	private Producer<String, byte[]> kafkaProducer;

	@RequestMapping("/download")
	public ResponseEntity<String> download(@RequestParam("type") String type, @RequestParam("date") String date)
			throws IOException, InterruptedException, ExecutionException {

		StringBuilder keyBuilder = new StringBuilder().append(type).append("/").append(date).append("/");
		String key = keyBuilder.toString();

		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket).withPrefix(key).withMaxKeys(2);
		ListObjectsV2Result result;

		do {
			result = s3Initializer.listObjectsV2(req);

			for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
				logger.info("Object summary key : " + objectSummary.getKey() + " , Size: " + objectSummary.getSize());

				GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, objectSummary.getKey());

				S3Object s3Object = s3Initializer.getObject(getObjectRequest);
				S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

				/*
				//Format 1: Have the content in file. No kafka serializer for this 
				//and hence if wanted this can be saved on the same machine or sent to HDFS for record keeping. 
				File file = new File(objectSummary.getKey());
				FileUtils.copyInputStreamToFile(objectInputStream, file);
				*/
				
				//Format 2: Convert to a byte array and use the byte array serializer.				
				byte [] inputByteArray = IOUtils.toByteArray(objectInputStream);

				// SEND TO KAFKA
				if ("true".equalsIgnoreCase(isKafkaEnabled)) {
					final ProducerRecord<String, byte[]> record = new ProducerRecord<String, byte[]>(
							DataConstants.TOPIC, inputByteArray);

					try {
						RecordMetadata metadata = kafkaProducer.send(record).get();
						logger.info("sent record : " + record.key() + ", partition : " + metadata.partition()
								+ ", metadata :" + metadata.offset());
					} catch (InterruptedException e) {
						logger.error("InterruptedException : ", e);
						throw e;
					} catch (ExecutionException e) {
						logger.error("ExecutionException : ", e);
						throw e;
					}
				}
			}

			String token = result.getNextContinuationToken();
			logger.info("Next Continuation Token: " + token);
			req.setContinuationToken(token);

		} while (result.isTruncated());

		return new ResponseEntity<String>("Data pushed to Kafka successfully", HttpStatus.OK);
	}

}
