package com.mcsds.cc.openaq.test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Paths;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class HadoopFSUtil {

	public static final String local_hdfsuri = "hdfs://localhost:9001/user/root/openaq";
	public static final String remote_hdfsuri = "hdfs://192.168.1.191:9003/user/root/openaq";

	public static final String dest_path = "user/root/openaq/realtime";
	public static final String src_path = "H:/backup/openaq-data/realtime/2013-11-26/2013-11-26.ndjson";

	private static final Logger logger = Logger.getLogger("com.mcsds.cc.openaq.test");

	public static void main(String[] args) {

		Configuration conf = new Configuration();
		conf.set("fs.defaultFS", remote_hdfsuri);
		conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());

		System.setProperty("HADOOP_USER_NAME", "root");

		String OS = System.getProperty("os.name").toLowerCase();
		if (OS.contains("win")) {
			System.setProperty("hadoop.home.dir", Paths.get("winutils").toAbsolutePath().toString());
		} else {
			System.setProperty("hadoop.home.dir", "/");
		}

		FSDataOutputStream out = null;
		FSDataInputStream in = null;
		

		try {
			
			InputStream is = new BufferedInputStream(new FileInputStream(src_path));
			
			FileSystem fs = FileSystem.get(URI.create(remote_hdfsuri), conf);
			out = fs.create(new Path(dest_path), Boolean.TRUE);

			logger.info("Begin Write file into hdfs");
			IOUtils.copyBytes(is, out, conf);
			logger.info("File read complete ..");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
